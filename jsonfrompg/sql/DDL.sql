drop table if exists title_basics;

create table title_basics ( tconst VARCHAR(20) primary key,
titleType VARCHAR(20),
primaryTitle VARCHAR(500),
originalTitle VARCHAR(500),
isAdult boolean ,
startYear INT ,
endYear INT ,
runtimeMinutes INT,
genres VARCHAR(200) );

drop table if exists title_akas;

create table title_akas ( titleId VARCHAR(20),
ordering INT ,
title VARCHAR(1000),
region VARCHAR(20),
language VARCHAR(20),
types VARCHAR(200),
attributes VARCHAR(200),
isOriginalTitle boolean ,
primary key (titleId,
ordering)
--foreign key (titleId) references title_basics (tconst)
)

drop table if exists name_basics;

create table name_basics(
nconst VARCHAR(20) primary key,
primaryName VARCHAR(500),
birthYear INT ,
deathYear INT ,
primaryProfession VARCHAR(500),
knownForTitles VARCHAR(500)
)
create table title_principals(
tconst VARCHAR(20),
ordering INT ,
nconst VARCHAR(20),
category VARCHAR(500),
job VARCHAR(500),
characters VARCHAR(500),
primary key (tconst,ordering)
)