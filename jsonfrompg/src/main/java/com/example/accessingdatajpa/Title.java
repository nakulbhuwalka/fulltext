package com.example.accessingdatajpa;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;

@Entity
@Table(name = "TITLE_BASICS")
public class Title {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private String tconst;
	private String titleType;
	private String primaryTitle;
	private String originalTitle;
	private Boolean isAdult;
	private Integer startYear;
//	@Column(name="endYear")
	private Integer endYear;
	private Integer runtimeMinutes;
	private String genres;
	
	@OneToMany(targetEntity = Principle.class, mappedBy = "tconst", fetch = FetchType.EAGER)
	//@Transient
	private List<Principle> principles;
	
	public String getTconst() {
		return tconst;
	}
	public void setTconst(String tconst) {
		this.tconst = tconst;
	}
	public String getTitleType() {
		return titleType;
	}
	public void setTitleType(String titleType) {
		this.titleType = titleType;
	}
	public String getPrimaryTitle() {
		return primaryTitle;
	}
	public void setPrimaryTitle(String primaryTitle) {
		this.primaryTitle = primaryTitle;
	}
	public String getOriginalTitle() {
		return originalTitle;
	}
	public void setOriginalTitle(String originalTitle) {
		this.originalTitle = originalTitle;
	}
	public Boolean getIsAdult() {
		return isAdult;
	}
	public void setIsAdult(Boolean isAdult) {
		this.isAdult = isAdult;
	}
	public Integer getStartYear() {
		return startYear;
	}
	public void setStartYear(Integer startYear) {
		this.startYear = startYear;
	}
	public Integer getEndYear() {
		return endYear;
	}
	public void setEndYear(Integer endYear) {
		this.endYear = endYear;
	}
	public Integer getRuntimeMinutes() {
		return runtimeMinutes;
	}
	public void setRuntimeMinutes(Integer runtimeMinutes) {
		this.runtimeMinutes = runtimeMinutes;
	}
	public String getGenres() {
		return genres;
	}
	public void setGenres(String genres) {
		this.genres = genres;
	}
	
	public List<Principle> getPrinciples() {
		return principles;
	}
	public void setPrinciples(List<Principle> principles) {
		this.principles = principles;
	}
	@Override
	public String toString() {
		return "Title [tconst=" + tconst + ", titleType=" + titleType + ", primaryTitle=" + primaryTitle
				+ ", originalTitle=" + originalTitle + ", isAdult=" + isAdult + ", startYear=" + startYear
				+ ", endYear=" + endYear + ", runtimeMinutes=" + runtimeMinutes + ", genres=" + genres + ", principles="
				+ principles + "]";
	}

	
	
}
