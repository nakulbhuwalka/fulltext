package com.example.accessingdatajpa;

import java.io.BufferedWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;


@SpringBootApplication
public class AccessingDataJpaApplication implements ApplicationRunner {

	@Autowired
	TitleRepository repository;

	public static void main(String[] args) {
		SpringApplication.run(AccessingDataJpaApplication.class, args);
	}

	@Override
	public void run(ApplicationArguments args) throws Exception {

		final ObjectMapper objectMapper = new ObjectMapper();
		// objectMapper.configure(SerializationFeature.INDENT_OUTPUT, true);
		Optional<Title> title = repository.findById("tt0000003");
		System.out.println(objectMapper.writeValueAsString(title.get()));
		System.out.println(title.get());
		BufferedWriter writer = Files.newBufferedWriter(Paths.get("e:\\data\\Movies3.txt"));
		long counter = 0;

		long size = repository.count();
		int pageSize = 10000;
		long pages = size / pageSize;
		System.out.println("total " + size);
		System.out.println("pages " + pages);
		for (int page = 72; page < pages; page++) {
			Pageable pageRequest = PageRequest.of(page, pageSize, Sort.by("tconst").descending());

			try {
				repository.findAll(pageRequest).forEach(t -> {
					try {

						writer.append(objectMapper.writeValueAsString(t));
						writer.newLine();
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}

				});
			} catch (Exception e) {
				e.printStackTrace();
			}
			System.out.println(page + "/" + pages);
			writer.flush();
		}

		writer.close();

	}

}
