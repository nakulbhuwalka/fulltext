package com.example.accessingdatajpa;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "title_principals")
public class Principle {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private String tconst;
	private Integer ordering;
	//private String nconst;
	private String category;
	private String job;
	private String characters;
	
	@ManyToOne(fetch = FetchType.EAGER )
	@JoinColumn( name = "nconst")
	private Name name;
	
	public String getTconst() {
		return tconst;
	}
	public void setTconst(String tconst) {
		this.tconst = tconst;
	}
	public Integer getOrdering() {
		return ordering;
	}
	public void setOrdering(Integer ordering) {
		this.ordering = ordering;
	}
	
	public String getCategory() {
		return category;
	}
	public void setCategory(String category) {
		this.category = category;
	}
	public String getJob() {
		return job;
	}
	public void setJob(String job) {
		this.job = job;
	}
	public String getCharacters() {
		return characters;
	}
	public void setCharacters(String characters) {
		this.characters = characters;
	}
	public Name getName() {
		return name;
	}
	public void setName(Name name) {
		this.name = name;
	}
	@Override
	public String toString() {
		return "Principle [tconst=" + tconst + ", ordering=" + ordering + ", category=" + category + ", job=" + job
				+ ", characters=" + characters + ", name=" + name + "]";
	}
	
	
	

}
