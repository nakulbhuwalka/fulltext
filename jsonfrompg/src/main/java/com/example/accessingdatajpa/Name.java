package com.example.accessingdatajpa;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
@Entity
@Table(name = "NAME_BASICS")
public class Name {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private String nconst;
	private String primaryName;
	private Integer birthYear;
	private Integer deathYear;
	private String primaryProfession;
	private String knownForTitles;
	public String getNconst() {
		return nconst;
	}
	public void setNconst(String nconst) {
		this.nconst = nconst;
	}
	public String getPrimaryName() {
		return primaryName;
	}
	public void setPrimaryName(String primaryName) {
		this.primaryName = primaryName;
	}
	public Integer getBirthYear() {
		return birthYear;
	}
	public void setBirthYear(Integer birthYear) {
		this.birthYear = birthYear;
	}
	public Integer getDeathYear() {
		return deathYear;
	}
	public void setDeathYear(Integer deathYear) {
		this.deathYear = deathYear;
	}
	public String getPrimaryProfession() {
		return primaryProfession;
	}
	public void setPrimaryProfession(String primaryProfession) {
		this.primaryProfession = primaryProfession;
	}
	public String getKnownForTitles() {
		return knownForTitles;
	}
	public void setKnownForTitles(String knownForTitles) {
		this.knownForTitles = knownForTitles;
	}
	@Override
	public String toString() {
		return "Name [nconst=" + nconst + ", primaryName=" + primaryName + ", birthYear=" + birthYear + ", deathYear="
				+ deathYear + ", primaryProfession=" + primaryProfession + ", knownForTitles=" + knownForTitles + "]";
	}
	
	
	
	
	
}
